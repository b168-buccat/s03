<?php

// Objects as Variables

$buildingObj = (object)[
	'name' => 'Caswynn Building',
	'floors' => 8,
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];

// Objects from Classes

class Building {
	public $name;
	public $floors;
	public $address;

	// Constructor is used during the creation of an object.

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function printName(){
		return "The name of the building is $this->name";
	}
}

class Condominium extends Building{
	public function printName(){
		return "The name of the condominium is $this->name";
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon city, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
